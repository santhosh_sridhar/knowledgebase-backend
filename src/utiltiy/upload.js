const { S3 } = require("aws-sdk");
const region = process.env.AWS_REGION;
const accessKeyId = process.env.ACCESS_KEY_ID;
const secretAccessKey = process.env.SECRET_ACESS_KEY;
const bucket = process.env.S3_BUCKET;

const uploadFile = (fileName, fileData) => {
  const s3 = new S3({ region: region, accessKeyId, secretAccessKey });

  return s3
    .upload({ Bucket: bucket, Key: fileName, Body: fileData })
    .promise()
    .then((res) => res.Key)
    .catch((err) => err);
};

module.exports = uploadFile;
