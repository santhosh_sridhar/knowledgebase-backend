const express = require("express");
const Mongoose = require("mongoose");
const userRoutes = require("./routes/user");
const categoryRoutes = require("./routes/category");
const { config } = require("dotenv");
const cookieParser = require("cookie-parser");

config();
const app = express();
const port = process.env.PORT || 8000;
const connectionString = process.env.DB_CONNECTION_STRING;

app.use(express.json());
app.use(express.urlencoded({ extended: true }));
app.use(cookieParser());
app.use("/api", userRoutes);
app.use("/api/category", categoryRoutes);
app.listen(port, () => {
  console.log(`server running at ${port}`);
  Mongoose.connect(connectionString, {
    useNewUrlParser: true,
    useUnifiedTopology: true,
    useCreateIndex: true,
  })
    .then((res) => {
      console.log("mongodb connected successfully");
    })
    .catch((err) => {
      console.log("mongodb connection failed");
      process.exit();
    });
});
