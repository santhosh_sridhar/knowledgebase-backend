const Mongoose = require("mongoose");
const bcrypt = require("bcryptjs");

const categorySchema = new Mongoose.Schema({
  name: { type: String, required: true, unique: true },
  content: { type: String },
  fileKey: { type: String },
});

const userSchema = new Mongoose.Schema({
  username: { type: String, required: true, unique: true },
  password: { type: String, required: true, min: 8, max: 16 },
  categories: [categorySchema],
});

userSchema.methods.encryptPassword = function (password) {
  const encryptedPassword = bcrypt.hashSync(password, bcrypt.genSaltSync(10));
  this.password = encryptedPassword;
};

userSchema.methods.validatePassword = function (password) {
  return bcrypt.compareSync(password, this.password);
};

module.exports = Mongoose.model("User", userSchema);
