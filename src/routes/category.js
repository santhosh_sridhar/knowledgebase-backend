const { Router } = require("express");
const jwt = require("jsonwebtoken");
const User = require("../model/user");
const multer = require("multer");
const uploadFile = require("../utiltiy/upload");

const SECRET = process.env.SECRET;
const router = Router();
const upload = multer();

async function authenticate(req, res, next) {
  const token = req.cookies.token;

  jwt.verify(token, SECRET, (err, decoded) => {
    if (err) res.status(401).json({ message: "Unauthorized User" });
    else next();
  });
}

router.get("/getCategories/:userid", authenticate, (req, res) => {
  User.findById(req.params.userid)
    .populate({ path: "categories", model: "Category", key: "user" })
    .exec((err, user) => {
      res.status(200).json({ data: user.categories });
    });
});

router.post("/addCategory", authenticate, (req, res) => {
  User.findById(req.body.userId)
    .then((user) => {
      user.categories.push({ name: req.body.name });
      return user.save();
    })
    .then((user) => {
      res.json({
        categories: user.categories,
        message: "Added category successully",
      });
    })
    .catch((err) => {
      res.status(500).json({ message: err.message });
    });
});

router.post(
  "/updateContent",
  authenticate,
  upload.single("file"),
  async (req, res) => {
    let updateStatement = {
      $set: {
        "categories.$.content": req.body.content,
      },
    };
    if (req.file) {
      const response = await uploadFile(file.originalname, file.buffer);
      if (response) updateStatement.$set["categories.$.fileKey"] = response;
    }
    User.findOneAndUpdate(
      { _id: req.body.userId, "categories._id": req.body.categoryId },
      updateStatement,
      { new: true }
    )
      .then((user) => {
        if (user) {
          res.json({
            categories: user.categories,
            message: "Updated Content Successfully",
          });
        } else {
          res.status(500).json({ message: "update content failed" });
        }
      })
      .catch((err) => {
        res.status(500).json({ message: err.message });
      });
  }
);

module.exports = router;
