const router = require("express").Router();
const User = require("../model/user");
const jwt = require("jsonwebtoken");
const secret = process.env.SECRET;

router.post("/signUp", (req, res) => {
  const user = new User({
    username: req.body.username,
  });
  user.encryptPassword(req.body.password);
  user.save().then((result) => {
    res.send({ status: 200, body: { message: "user created successfully" } });
  });
});

router.post("/signIn", (req, res) => {
  User.findOne({ username: req.body.username }).then((user) => {
    if (user) {
      const valid = user.validatePassword(req.body.password);

      if (valid) {
        res.cookie(
          "token",
          jwt.sign({ id: user.id }, secret, { expiresIn: 30000 }),
          { httpOnly: true, sameSite: true }
        );
        res.json({
          message: "user signIn successful",
          user,
        });
      } else {
        res.status(401).json({ message: "Invalid Password" });
      }
    } else {
      res.status(400).json({ message: "User not found" });
    }
  });
});

module.exports = router;
